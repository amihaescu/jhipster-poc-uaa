package ro.deloitte.digital.jhipster.uaa.repository;

import ro.deloitte.digital.jhipster.uaa.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
