/**
 * View Models used by Spring MVC REST controllers.
 */
package ro.deloitte.digital.jhipster.uaa.web.rest.vm;
